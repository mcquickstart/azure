resource "random_pet" "rg-name" {
  prefix    = var.rg_name_prefix
}

resource "azurerm_resource_group" "rg" {
  name      = random_pet.rg-name.id
  location  = var.rg_loction
  tags      = var.rg_tags
}