terraform {
    required_version = ">=0.12"
    required_providers {
        azurerm={
            source = "hashicorp/azurerm"
            version = "~>2.0"
        }
    }
}

provider "azurerm" {
    features {}
    subscription_id = "Enter your subscription id"
    client_id = "Enter your service principal id."
    client_certificate_path = "Enter the path of the certificate."
    client_certificate_password = "Enter the password to acces the certiificate file"
    tenant_id = "Specify the tenant id."
}