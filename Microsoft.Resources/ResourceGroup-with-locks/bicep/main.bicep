//.......................main.bicep

//Setting the target scope
targetScope = 'subscription'

//Specify the name of the resource group
@minLength(3)
@maxLength(25)
@description('Specify the name of the resource group')
param resourceGroupName string

//Specify the location of the resource group
@allowed([
  'eastasia'
  'southeastasia'
  'centralus'
  'eastus'
  'eastus2'
  'westus'
  'northcentralus'
  'southcentralus'
  'northeurope'
  'westeurope'
  'japanwest'
  'japaneast'
  'brazilsouth'
  'australiaeast'
  'australiasoutheast'
  'southindia'
  'centralindia'
  'westindia'
  'canadacentral'
  'canadaeast'
  'uksouth'
  'ukwest'
  'westcentralus'
  'westus2'
  'koreacentral'
  'koreasouth'
  'francecentral'
  'francesouth'
  'australiacentral'
  'australiacentral2'
  'uaecentral'
  'uaenorth'
  'southafricanorth'
  'southafricawest'
  'switzerlandnorth'
  'switzerlandwest'
  'germanynorth'
  'germanywestcentral'
  'norwaywest'
  'norwayeast'
  'brazilsoutheast'
  'westus3'
  'swedencentral'
])
@description('Specify the location of the resource group')
param resourceGroupLocation string

//Specify the tags
@description('Specify the tags')
param resourceGroupTags object

//Specify  the lock type
@allowed([
  'CannotDelete'
  'ReadOnly'
])
@description('Specify  the lock type')
param resourceGroupLock string

//Creating a resource group
resource rg 'Microsoft.Resources/resourceGroups@2021-01-01' = {
  name: resourceGroupName
  location:resourceGroupLocation
  tags:resourceGroupTags
}

module deployRgLock './LockResourceGroup.bicep' = {
  name: 'lockDeployment'
  scope: resourceGroup(rg.name)
  params:{
    resourceGroupLock:resourceGroupLock
  }
}
