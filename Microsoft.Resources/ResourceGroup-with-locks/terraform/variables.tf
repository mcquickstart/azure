variable "rg_name_prefix" {
    type = string
    description = "Specify the prefix of the resource group name. It will be combined with a random id to generate a unique resource group name."
    default = "rg"
}

variable "rg_loction" {
    type = string
    description = "Specify the location of the resource group"
    default = "centralindia"
}

variable "rg_tags" {
    type = map(string)
    description = "Specify the tags of the resourc group."
    default = {}
}

variable "rg_lock" {
    type = string
    description = "Specify the type of the lock."
    default = ""
}