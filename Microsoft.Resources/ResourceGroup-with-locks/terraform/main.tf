resource "random_pet" "rg-name" {
  prefix    = var.rg_name_prefix
}

resource "azurerm_resource_group" "rg" {
  name      = random_pet.rg-name.id
  location  = var.rg_loction
  tags      = var.rg_tags
}

resource "azurerm_management_lock" "rgLock" {
  name = random_pet.rg-name.id
  scope = azurerm_resource_group.rg.id
  lock_level = var.rg_lock
}